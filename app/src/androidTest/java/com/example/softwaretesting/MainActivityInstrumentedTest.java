package com.example.softwaretesting;


import android.view.View;
import android.widget.ListView;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityInstrumentedTest {

    private List<String> numbersAsString = Arrays.asList("12", "5", "25");
    private List<String> numbersAsBinary = Arrays.asList("1100", "101", "11001");

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void before() {
        onView(withId(R.id.input))
                .perform(clearText(), typeText(numbersAsString.get(0)), closeSoftKeyboard());
        onView(withId(R.id.button_1)).perform(click());
        onView(withId(R.id.input))
                .perform(clearText(), typeText(numbersAsString.get(1)), closeSoftKeyboard());
        onView(withId(R.id.button_1)).perform(click());
    }

    @Test
    public void inputText_addsNumber() {
        onView(withId(R.id.input))
                .perform(clearText(), typeText(numbersAsString.get(2)), closeSoftKeyboard());
        onView(withId(R.id.button_1)).perform(click());
        onData(anything())
                .inAdapterView(withId(R.id.list_view))
                .atPosition(2)
                .check(matches(withText(numbersAsBinary.get(2))));
    }

    @Test
    public void sortButton_sortsListView() {
        onView(withId(R.id.button_2)).perform(click());
        onData(anything())
                .inAdapterView(withId(R.id.list_view))
                .atPosition(0)
                .check(matches(withText(numbersAsBinary.get(1))));
        onData(anything())
                .inAdapterView(withId(R.id.list_view))
                .atPosition(1)
                .check(matches(withText(numbersAsBinary.get(0))));
    }

    @Test
    public void clearButton_clearsListView() {
        onView(withId(R.id.button_3)).perform(click());
        onView(withId(R.id.list_view))
                .check(matches(new TypeSafeMatcher<View>() {
                    @Override
                    protected boolean matchesSafely(View item) {
                        return ((ListView) item).getAdapter().getCount() == 0;
                    }

                    @Override
                    public void describeTo(Description description) {
                        description.appendText("ListView should be empty");
                    }
                }));
    }

    @Test
    public void selectRadix_changesListView() {
        onView(withId(R.id.radix_spinner)).perform(click());
        onData(anything()).atPosition(6).perform(click());

        onView(withId(R.id.radix_spinner)).check(matches(withSpinnerText("10")));

        onData(anything())
                .inAdapterView(withId(R.id.list_view))
                .atPosition(0)
                .check(matches(withText(numbersAsString.get(0))));
        onData(anything())
                .inAdapterView(withId(R.id.list_view))
                .atPosition(1)
                .check(matches(withText(numbersAsString.get(1))));
    }

}
