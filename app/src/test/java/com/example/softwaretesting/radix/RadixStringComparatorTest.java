package com.example.softwaretesting.radix;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RadixStringComparatorTest {

    private RadixStringComparator subject;

    @Before
    public void before() {
        subject = new RadixStringComparator(2);
    }

    @Test
    public void test_setRadix() {
        int radix = 10;
        subject.setRadix(radix);
        String four = "4";
        String five = "5";
        assertEquals(- 1, subject.compare(four, five));
    }

    @Test
    public void test_compare() {
        String fourBinary = "100";
        String fiveBinary = "101";
        assertEquals(- 1, subject.compare(fourBinary, fiveBinary));
    }

}
