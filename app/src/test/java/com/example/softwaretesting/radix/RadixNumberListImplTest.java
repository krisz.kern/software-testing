package com.example.softwaretesting.radix;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RadixNumberListImplTest {

    private RadixNumberListImpl subject;

    @Before
    public void before() {
        subject = new RadixNumberListImpl(2);
    }

    @Test
    public void test_getSetRadix() {
        assertEquals(new Integer(2), subject.getRadix());
        subject.setRadix(10);
        assertEquals(new Integer(10), subject.getRadix());
    }

    @Test
    public void test_add() {
        subject.add("2");
        assertEquals("10", subject.getNumbers().get(0));
    }

    @Test
    public void test_setRadixConvertsNumbers() {
        subject.add("2");
        assertEquals("10", subject.getNumbers().get(0));
        subject.setRadix(10);
        assertEquals("2", subject.getNumbers().get(0));
    }

    @Test
    public void test_sort() {
        subject.add("3");
        subject.add("2");
        subject.sort();
        assertEquals("10", subject.getNumbers().get(0));
        assertEquals("11", subject.getNumbers().get(1));
    }

    @Test
    public void test_clear() {
        subject.add("2");
        assertEquals(1, subject.getNumbers().size());
        subject.clear();
        assertTrue(subject.getNumbers().isEmpty());
    }

}
