package com.example.softwaretesting;

import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.example.softwaretesting.radix.RadixNumberList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainActivityUnitTest {

    @InjectMocks
    private MainActivity mainActivity;
    @Mock
    private EditText input;
    @Mock
    private ArrayAdapter<String> adapter;
    @Mock
    private RadixNumberList radixNumberList;

    @Test
    public void test_onAdd() {
        Editable mockEditable = mock(Editable.class);
        String number = "25";
        when(mockEditable.toString()).thenReturn(number);
        when(input.getText()).thenReturn(mockEditable);

        mainActivity.onAdd(mock(View.class));

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(radixNumberList).add(captor.capture());
        assertEquals(number, captor.getValue());
        verify(adapter).notifyDataSetChanged();
    }

    @Test
    public void test_onSort() {
        mainActivity.onSort(mock(View.class));

        verify(radixNumberList).sort();
        verify(adapter).notifyDataSetChanged();
    }

    @Test
    public void test_onClear() {
        mainActivity.onClear(mock(View.class));

        verify(radixNumberList).clear();
        verify(adapter).notifyDataSetChanged();
    }

    @Test
    public void test_onItemSelected() {
        AdapterView<?> adapterView = mock(AdapterView.class);
        when(adapterView.getItemAtPosition(1)).thenReturn("3");
        mainActivity.onItemSelected(adapterView, mock(View.class), 1, 1);

        verify(radixNumberList).setRadix(3);
        verify(adapter).notifyDataSetChanged();
    }

}