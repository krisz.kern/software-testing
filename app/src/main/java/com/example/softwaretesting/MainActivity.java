package com.example.softwaretesting;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.softwaretesting.radix.RadixNumberList;
import com.example.softwaretesting.radix.RadixNumberListImpl;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

	private EditText input;
	private ArrayAdapter<String> listViewAdapter;

	private RadixNumberList radixNumberList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final Spinner spinner = findViewById(R.id.radix_spinner);
		final ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
				R.array.radix_array, android.R.layout.simple_spinner_item);
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(spinnerAdapter);
		spinner.setOnItemSelectedListener(this);

		radixNumberList = new RadixNumberListImpl(2);

		input = findViewById(R.id.input);
		final ListView listView = findViewById(R.id.list_view);
		listViewAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
				radixNumberList.getNumbers());
		listView.setAdapter(listViewAdapter);
	}

	public void onSort(View view) {
		radixNumberList.sort();
		listViewAdapter.notifyDataSetChanged();
	}

	public void onClear(View view) {
		radixNumberList.clear();
		listViewAdapter.notifyDataSetChanged();
	}

	public void onAdd(View view) {
		radixNumberList.add(input.getText().toString());
		listViewAdapter.notifyDataSetChanged();
	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
		String radix = (String) adapterView.getItemAtPosition(pos);
		radixNumberList.setRadix(Integer.parseInt(radix));
		listViewAdapter.notifyDataSetChanged();
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {
		// Do nothing
	}

}
