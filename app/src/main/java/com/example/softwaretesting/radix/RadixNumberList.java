package com.example.softwaretesting.radix;

import java.util.List;

public interface RadixNumberList {

    List<String> getNumbers();

    Integer getRadix();

    void setRadix(Integer radix);

    void add(String decimal);

    void sort();

    void clear();

}
