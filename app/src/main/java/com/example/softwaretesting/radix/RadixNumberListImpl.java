package com.example.softwaretesting.radix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RadixNumberListImpl implements RadixNumberList {

    private Integer radix;

    private List<String> numbers;

    private RadixStringComparator comparator;

    public RadixNumberListImpl(Integer radix) {
        this.radix = radix;
        this.numbers = new ArrayList<>();
        this.comparator = new RadixStringComparator(radix);
    }

    @Override
    public List<String> getNumbers() {
        return this.numbers;
    }

    @Override
    public Integer getRadix() {
        return this.radix;
    }

    @Override
    public void setRadix(Integer radix) {
        int previousRadix = this.radix;
        this.radix = radix;
        this.comparator.setRadix(radix);
        convertToNewRadix(previousRadix);
    }

    private void convertToNewRadix(Integer previousRadix) {
        final List<String> list = new ArrayList<>();
        for (String number : this.numbers) {
            int n = Integer.parseInt(number, previousRadix);
            list.add(Integer.toString(n, this.radix));
        }
        this.numbers.clear();
        this.numbers.addAll(list);
    }

    @Override
    public void add(String number) {
        if (number == null || number.isEmpty()) {
            return;
        }
        int n = Integer.parseInt(number);
        String radixForm = Integer.toString(n, radix);
        numbers.add(radixForm);
    }

    @Override
    public void sort() {
        Collections.sort(numbers, comparator);
    }

    @Override
    public void clear() {
        numbers.clear();
    }

}
