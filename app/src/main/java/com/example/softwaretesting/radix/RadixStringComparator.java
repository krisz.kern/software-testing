package com.example.softwaretesting.radix;

import java.util.Comparator;

public class RadixStringComparator implements Comparator<String> {

	private Integer radix;

	public RadixStringComparator(Integer radix) {
		this.radix = radix;
	}

	public void setRadix(Integer radix) {
		this.radix = radix;
	}

	@Override
	public int compare(final String o1, final String o2) {
		return Integer.compare(Integer.parseInt(o1, radix), Integer.parseInt(o2, radix));
	}

}
